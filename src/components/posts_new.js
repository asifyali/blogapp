import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createPost } from '../actions';

class PostsNew extends Component {

    renderField(field) {

        const {meta: { touched, error }} = field;
        const className = `form-group ${touched && error ? 'has-danger':''}`
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input className="form-control" type="text" {... field.input} />
                <div className="text-help">
                    { touched ? error:'' }
                </div>
            </div>
        );
    }

    onSubmit(values) {
        this.props.createPost(values, () => {
            this.props.history.push('/');
        });
    }

    render() {

        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field 
                name="title"
                label="Title for Post"
                component={this.renderField}
                />
                <Field 
                name="categories"
                label="Categories"
                component={this.renderField}
                /> 
                <Field 
                name="content"
                label="Post Content"
                component={this.renderField}
                />
                <button type="submit"  className="btn btn-primary">Submit</button> 
                <Link className="btn btn-danger" to="/">Cancel</Link>
           
            </form>
        );
    }
}

function validate (values) {

    const errors = {};

    if (!values.title) {
        errors.title = "Please enter a title";
    }
    if (!values.categories) {
        errors.categories = "Please enter a Category";
    }
    if (!values.content) {
        errors.content = "Please enter some content please";
    }
    return errors;
}
export default reduxForm({
    validate: validate,
    form: 'PostsNewForm'
})
(
    connect(null,{ createPost } )(PostsNew)
);